package com.sysomos.neuralnet.inference.data;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.fetcher.BaseDataFetcher;

import com.sysomos.neuralnet.inference.exception.DataFetcherException;

public class InferenceDataFetcher extends BaseDataFetcher {

	private static final long serialVersionUID = -6698461759084774657L;
	private List<DataSet> examples;

	public InferenceDataFetcher(int inputColumns, int numOutcomes,
			List<DataSet> examples) {
		this.examples = examples;
		this.numOutcomes = numOutcomes;
		this.inputColumns = inputColumns;
		this.totalExamples = examples == null ? -1 : examples.size();
	}

	public InferenceDataFetcher() throws DataFetcherException {
		initialize();
	}

	private void initialize() throws DataFetcherException {
		Properties prop = new Properties();

		InputStream inputStream = getClass().getClassLoader()
				.getResourceAsStream("model.properties");
		try {
			prop.load(inputStream);
		} catch (IOException e) {
			throw new DataFetcherException(e);
		}

		numOutcomes = Integer.parseInt(prop.getProperty("numOutcomes"));
		inputColumns = Integer.parseInt(prop.getProperty("inputColumns"));
		totalExamples = Integer.parseInt(prop.getProperty("totalExamples"));
	}

	@Override
	public void fetch(int numExamples) {
		initializeCurrFromList(examples);
	}

}
